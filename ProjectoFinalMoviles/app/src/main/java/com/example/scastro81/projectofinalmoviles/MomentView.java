package com.example.scastro81.projectofinalmoviles;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MomentView extends AppCompatActivity {

    TextView name, address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moment_view);

        name = (TextView)findViewById(R.id.nametxt);
        address = (TextView)findViewById(R.id.addresstxt);

        Intent i = getIntent();

        name.setText("Name: "+i.getStringExtra("name"));
        address.setText("Address: "+i.getStringExtra("address"));

    }
}
