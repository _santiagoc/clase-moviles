package com.example.scastro81.projectofinalmoviles;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, JSONRequest.JSONRequestCallback,
        AdapterView.OnItemClickListener{

    private static final String SELECTED_ITEM = "arg_selected_item";

    private BottomNavigationView mBottomNav;
    private int mSelectedItem;

    //JSON
    private JSONRequest j;
    private JSONArray jarray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBottomNav = (BottomNavigationView) findViewById(R.id.navigation);
        mBottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                selectFragment(item);
                return true;
            }
        });

        MenuItem selectedItem;
        if (savedInstanceState != null) {
            mSelectedItem = savedInstanceState.getInt(SELECTED_ITEM, 0);
            selectedItem = mBottomNav.getMenu().findItem(mSelectedItem);
        } else {
            selectedItem = mBottomNav.getMenu().getItem(0);
        }
        selectFragment(selectedItem);

        //JSON
        j = new JSONRequest(this);

    }

    public void load(View v){
        j.execute("https://api.myjson.com/bins/1c8b1n");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(SELECTED_ITEM, mSelectedItem);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        MenuItem homeItem = mBottomNav.getMenu().getItem(0);
        if (mSelectedItem != homeItem.getItemId()) {
            // select home item
            selectFragment(homeItem);
        } else {
            super.onBackPressed();
        }
    }

    private void selectFragment(MenuItem item) {
        Fragment frag = null;
        // init corresponding fragment
        frag = Fragment_Dashboard.newInstance(getString(R.string.text_dashboard), getColorFromRes(R.color.color_dashboard));
        //frag = Fragment_Team.newInstance(getString(R.string.text_team), getColorFromRes(R.color.color_team));

        switch (item.getItemId()) {
            case R.id.menu_add:
                //frag = Fragment_Add.newInstance(getString(R.string.text_add),getColorFromRes(R.color.color_add));
                break;
            case R.id.menu_dashboard:
                //frag = Fragment_Dashboard.newInstance(getString(R.string.text_dashboard), getColorFromRes(R.color.color_dashboard));
                break;
            case R.id.menu_team:
                //frag = Fragment_Team.newInstance(getString(R.string.text_team), getColorFromRes(R.color.color_team));
                break;
        }

        // update selected item
        mSelectedItem = item.getItemId();

        // uncheck the other items.
        for (int i = 0; i< mBottomNav.getMenu().size(); i++) {
            MenuItem menuItem = mBottomNav.getMenu().getItem(i);
            menuItem.setChecked(menuItem.getItemId() == item.getItemId());
        }

        updateToolbarText(item.getTitle());

        if (frag != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(R.id.container, frag, frag.getTag());
            ft.commit();
        }
    }

    private void updateToolbarText(CharSequence text) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(text);
        }
    }

    private int getColorFromRes(@ColorRes int resId) {
        return ContextCompat.getColor(this, resId);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void done(JSONArray jsonArray) {
        jarray = jsonArray;
        JSONAdapter jadapter = new JSONAdapter(jarray, this);
        ListView lv = (ListView)findViewById(R.id.listView);
        lv.setAdapter(jadapter);
        lv.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent i = new Intent(this, MomentView.class);
        try{
            i.putExtra("name", jarray.getJSONObject(position).getString("name"));
            i.putExtra("address", jarray.getJSONObject(position).getString("address"));
        }catch(JSONException j){
            j.printStackTrace();
        }

    }
}
