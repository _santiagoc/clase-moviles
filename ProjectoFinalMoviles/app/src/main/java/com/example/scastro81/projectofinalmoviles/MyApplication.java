package com.example.scastro81.projectofinalmoviles;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

/**
 * Created by scastro81 on 7/04/17.
 */

public class MyApplication extends Application{

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
