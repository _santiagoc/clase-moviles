package com.example.scastro81.projectofinalmoviles;

import android.content.Intent;
import android.os.PersistableBundle;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Config;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by scastro81 on 5/04/17.
 */

public class CompleteRegistration extends AppCompatActivity implements View.OnClickListener{

    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;

    private EditText editTextNombre;
    private EditText editTextMatricula;
    private Button btnAceptar;
    private Button btnCancelar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completereg);

        //initializing firebase authentication object
        firebaseAuth = FirebaseAuth.getInstance();

        //if the user is not logged in
        //that means current user will return null
        if(firebaseAuth.getCurrentUser() == null){
            //closing this activity and open Login
            finish();
            //startActivity(new Intent(this, LoginActivity.class));
        }

        databaseReference = FirebaseDatabase.getInstance().getReference();

        editTextNombre = (EditText) findViewById(R.id.editTextNombre);
        editTextMatricula = (EditText) findViewById(R.id.editTextMatricula);
        btnAceptar = (Button) findViewById(R.id.btnAceptar);
        btnCancelar = (Button) findViewById(R.id.btnCancelar);

        //getting current user
        FirebaseUser user = firebaseAuth.getCurrentUser();

        btnCancelar.setOnClickListener(this);
        btnAceptar.setOnClickListener(this);

        Firebase.setAndroidContext(this);

    }

    public void complete(){
        //Creating firebase object
        Firebase ref = new Firebase(com.example.scastro81.projectofinalmoviles.Config.FIREBASE_URL);

        String nombre = editTextNombre.getText().toString().trim();
        String matricula = editTextMatricula.getText().toString().trim();

        String uid = firebaseAuth.getCurrentUser().getUid();

        User user = new User();

        user.setNombre(nombre);
        user.setMatricula(matricula);

        ref.child("Users").child(uid).setValue(user);
        Toast.makeText(this, "Updating data...",Toast.LENGTH_LONG).show();

    }

    @Override
    public void onClick(View view) {
        //if logout is pressed
        if(view == btnCancelar){
            //logging out the user
            firebaseAuth.signOut();
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        if(view == btnAceptar){
            complete();
            finish();
            startActivity(new Intent(this, MainActivity.class));
        }
    }


}
