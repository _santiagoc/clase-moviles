package com.example.moviles.clase3_3feb17;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private DBHelper db;
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new DBHelper(this);
        editText = (EditText)findViewById(R.id.editText);
    }

    public void add(View v){
        db.add(editText.getText().toString());
        Toast.makeText(this, "RECORD ADDED", Toast.LENGTH_SHORT).show();
    }

    public void find(View v){
        Toast.makeText(this, "found: " + db.find(editText.getText().toString()), Toast.LENGTH_SHORT).show();
    }

    public void delete(View v){
        Toast.makeText(this, "delete: " + db.find(editText.getText().toString()), Toast.LENGTH_SHORT).show();
    }
}
