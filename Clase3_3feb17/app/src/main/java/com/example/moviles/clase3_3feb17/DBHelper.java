package com.example.moviles.clase3_3feb17;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by moviles on 2/3/17.
 */
public class DBHelper extends SQLiteOpenHelper{

    public static final String DB_NAME = "school.db";
    public static final int DB_VERSION = 1;

    public static final String TABLE_NAME = "student";
    public static final String ID_FIELD = "id";
    public static final String NAME_FIELD = "name";

    public DBHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);

    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        //SQL table creation
        String creation = "CREATE TABLE " + TABLE_NAME + "( " + ID_FIELD + " INTEGER PRIMARY KEY, " + NAME_FIELD + " TEXT)";
        db.execSQL(creation);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String[] name = {TABLE_NAME};

        //prepared statements
        db.execSQL("DROP TABLE IF EXISTS ?", name);
        onCreate(db);
    }

    public void add(String name){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv =  new ContentValues();

        //se especifica el atributo aqui
        cv.put(NAME_FIELD, name);
        db.insert(TABLE_NAME, null, cv);
    }
    public int delete(String name){

        SQLiteDatabase db = getWritableDatabase();

        //aqui iria la clausula AND, OR, etc
        String clause = NAME_FIELD + " = ?";
        String[] args = {name};

        return db.delete(TABLE_NAME, clause, args);
    }
    public int find(String name){

        SQLiteDatabase db = getWritableDatabase();

        //aqui iria la clausula AND, OR, etc
        String selection = NAME_FIELD + " = ?";
        String[] args = {name};

        Cursor c = db.query(TABLE_NAME, null, selection, args, null, null, null, null);
        int result = -1;

        if(c.moveToFirst()){
            result = c.getInt(0);
        }

        return result;
    }
}
